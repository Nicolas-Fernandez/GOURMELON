# GOURMELON
## Aim:
Re-run GOURMELON mothur workflow first used with (article1) for (article2)

## Article1
### Environmental and geographical factors structure soil microbial diversity on New Caledonian ultramafic substrates: a metagenomic approach.
Véronique Gourmelon, Laurent Maggia, Jeff R Powell, Sarah Gigante, Sara Horta, Claire Gueunier, Kelly Letellier & Fabian Carriconde. PLoS ONE - 2016

## Article2
