"""
############################################################################################
######################################### Snakefile ########################################
############################################################################################

Author:
    Nicolas FERNANDEZ
Create:
    2017-12-13
LastUpdates:
    2017-12-22
Aim:
    Metabarcoding Mothur piepeline (Veronique Thesis)
Use:
    dir:
        inp:
            reads: Sample.fq
            mothur: UNITE-Ref-Seq.fasta ; UNITE-Ref-Tax.txt
        out:
            mothur: {dataset}.file
Primers:
    ITS2:
        its7-F:
        its4-R:
Dataset:
    Kopeto & Riviere Blanche: Kop & RB
        Veronique GOURMELON - 32 samples - ['S','Mq','Na','M']
    Tiebaghi: T
        Julien DEMENOIS - 9 samples - ['MqA','MqAD','MqP']
    Bois du Sud: BDS
        Veronique GOURMELON - 4 samples - ['Ag']
Runs:
    Sequencer:
        MiSeq Illumina
    Runs:
        2
    Paired-End:
        251 / 251 nt

############################################################################################
######################################## To Do List  #######################################
############################################################################################

- Change output dir for each {dataset}
- Change input dir for {dataset}.file
- Fix log
- Deal with dynamic UNITE

############################################################################################
"""


############################################################################################
######################################### Functions ########################################
############################################################################################

# For all rules
OUTPUTDIR = '/home/fernandez/theses_Julien_Vero/out/mothur' # output directory

# For rule screen_seqs
MAXAMBIG = 0    # max ambiguous bases allowed
MAXHOMOP = 8    # max homopolymere allowed
MINLENGTH = 150 # min sequence size

# For rule chimera_uchime
REFERENCE = 'self' # ! WARNING ! when using 'self', mothur can only use 1 processor...
                   # Can be replace with online database i.e. UNITE, GreenGene, Silva...

# For rule sub_sample                   
PERSAMPLE = 'true' # can be false...

# For rule pairwise_seqs
CUTOFF = 0.10   # allow 10% error bases
CALC = 'onegap' # missing bases are considered like one unique gap
COUNTENDS = 'F' # gaps in sequence end are ignored

# For rule cluster
METHOD='furthest' # clustering start from furthest sequences ; can be change for 'nearest'
SHOWABUND='f'     # don't show each cluster in a file ; avoid computer memory overcharge

# For rule classify_seqs
TEMPLATE = 'inp/mothur/UNITE_public_mothur_10.10.2017.fasta'              # UNITE database 2017
TAXONOMY = 'inp/mothur/UNITE_public_mothur_10.10.2017_taxonomy.txt'       # UNITE database 2017
#TEMPLATE = 'inp/mothur/UNITE_public_mothur_full_02.03.2015_taxonomy.txt' # UNITE database 2015
#TAXONOMY = 'inp/mothur/UNITE_public_mothur_full_02.03.2015.fasta'        # UNITE database 2015

# For all rules "taxonoymy"
LABEL = ['0.01'] # Distance levels you would like, can be multiple

############################################################################################
######################################### Variables ########################################
############################################################################################

#DATASET, = glob_wildcards('inp/mothur/{dataset}.file')
DATASET = ['KRBTBSwoSrb']



############################################################################################
###################################### Glob_wildcards ######################################
############################################################################################

rule all:
    input:
        # Output rule 18_get_oturep:
        ### Mothur's sequence name
        RepName=expand('out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.{label}.rep.names', dataset=DATASET, label=LABEL),
        ### Sequence, Mothur's sequence name, sequence count, sample with this sequence
        RepFasta=expand('out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.{label}.rep.fasta', dataset=DATASET, label=LABEL),
        # Output rule 17_get_relabund:
        ### Distance matrix for each OTU and each sample
        FnRelAbund=expand('out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.relabund', dataset=DATASET),
        # Output rule 16_make_share --> Input rule 17
        # Output rule 15_classify_otu:
        ### OTU name and OTU/taxonomy's representative sequences number
        ConsTaxonomy=expand('out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.{label}.cons.taxonomy', dataset=DATASET, label=LABEL),
        ### Taxonomie (from kind to species) and level's representative sequences number per sample
        ConsTaxSummary=expand('out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.{label}.cons.tax.summary', dataset=DATASET, label=LABEL),
        # Output rule 14_classify_seqs --> Input rule 15
        # Output rule 13_cluster --> Input rule 15, 16 and 18
        # Output rule 12_pairwise_seqs --> Input rule 13 and 18
        # Output rule 11_subsample_count_groups:
        SubsampleCountSummary=expand('out/mothur/{dataset}/{dataset}.contigs.good.pick.subsample.count.summary', dataset=DATASET),
        # Output rule 10_sub_sample --> Input rules 11 to 16 and 18 
        # Output rule 09_pick_groups_count:
        PickCountSummary=expand('out/mothur/{dataset}/{dataset}.contigs.good.pick.count.summary',
                                dataset=DATASET),
        # Output rule 08_remove_seqs --> Input rules 09 and 10
        # Output rule 06_unique_seqs --> Input rules 07 and 08
        # Output rule 05_count_group:
        CountSummary=expand('out/mothur/{dataset}/{dataset}.contigs.good.count.summary',
                            dataset=DATASET),
        # Output rule 04_good_summary_seqs:
        GoodSummary=expand('out/mothur/{dataset}/{dataset}.trim.contigs.good.summary',
                           dataset=DATASET),
        # Output rule 03_screen_seqs --> Input rules 04 to 06
        # Output rule 02_summary_seqs:
        ContigSummary=expand('out/mothur/{dataset}/{dataset}.trim.contigs.summary',
                             dataset=DATASET)
        # Output rule 01_make_contigs --> Input rule 02
        
############################################################################################
################################# One Rule to rule them all ################################
############################################################################################

rule get_oturep:
    #Rule 18.0
    #Author: Nicolas FERNANDEZ
    #Date: 14-12-2017
    #LastUpdate: 14-12-2017
    #Aim: Export abund
    #Use: mothur "#get.oturep()"
    threads: 8
    log:
        'log/get-oturep.log'
    params:
        outputdir=OUTPUTDIR # output directory
    message:
        'Export fasta'
    input:
        DistanceMatrix='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.dist',
        SubsampleNames='out/mothur/{dataset}/{dataset}.trim.contigs.good.pick.subsample.names',
        FnList='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.list',
        SubsampleGroups='out/mothur/{dataset}/{dataset}.contigs.good.pick.subsample.groups',
        SubsampleFasta='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fasta'
    output:
        RepName='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.{label}.rep.names',
        RepFasta='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.{label}.rep.fasta'
    shell:
        'mothur \"# '
        'get.oturep( '
        'column={input.DistanceMatrix}, '
        'name={input.SubsampleNames}, '
        'list={input.FnList}, '
        'label={wildcards.label}, '
        'group={input.SubsampleGroups}, '
        'fasta={input.SubsampleFasta}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'

############################################################################################

rule get_relabund:
    #Rule 17.0
    #Author: Nicolas FERNANDEZ
    #Date: 14-12-2017
    #LastUpdate: 14-12-2017
    #Aim: Export abund
    #Use: mothur "#get.relabund()"
    threads: 8
    log:
        'log/get-relabund.log'
    params:
        outputdir=OUTPUTDIR # output directory
    message:
        'Export abund'
    input:
        FnShared='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.shared'
    output:
        FnRelAbund='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.relabund'
    shell:
        'mothur \"# '
        'get.relabund( '
        'shared={input.FnShared}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'
        
############################################################################################

rule make_share:
    #Rule 16.0
    #Author: Nicolas FERNANDEZ
    #Date: 14-12-2017
    #LastUpdate: 14-12-2017
    #Aim: Read a list and group file and creates a .shared file for each group.
    #Use: mothur "#make.shared()"
    threads: 8
    log:
        'log/make-share.log'
    params:
        outputdir=OUTPUTDIR, # output directory
        label=LABEL
    message:
        'Make share'
    input:
        FnList='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.list',
        SubsampleGroups='out/mothur/{dataset}/{dataset}.contigs.good.pick.subsample.groups'
    output:
        FnShared='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.shared'
    shell:
        'mothur \"# '
        'make.shared( '
        'list={input.FnList}, '
        'label={params.label}, '
        'group={input.SubsampleGroups}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'

############################################################################################

rule classify_otu:
    #Rule 15.0
    #Author: Nicolas FERNANDEZ
    #Date: 14-12-2017
    #LastUpdate: 14-12-2017
    #Aim: Taxonomic asignations
    #Use: mothur "#classify.otu()"
    threads: 8
    log:
        'log/classify-otu.log'
    params:
        outputdir=OUTPUTDIR # output directory
    message:
        'Taxonomic asignations'
    input:
        WangTaxonomy='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.2017_taxonomy.wang.taxonomy',
        FnList='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.list',
        SubsampleNames='out/mothur/{dataset}/{dataset}.trim.contigs.good.pick.subsample.names',
        SubsampleGroups='out/mothur/{dataset}/{dataset}.contigs.good.pick.subsample.groups'
    output:
        ConsTaxonomy='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.{label}.cons.taxonomy',
        ConsTaxSummary='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.{label}.cons.tax.summary'
    shell:
        'mothur \"# '
        'classify.otu( '
        'taxonomy={input.WangTaxonomy}, '
        'list={input.FnList}, '
        'label={wildcards.label}, '
        'name={input.SubsampleNames}, '
        'group={input.SubsampleGroups}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'

############################################################################################

rule classify_seqs:
    #Rule 14.0
    #Author: Nicolas FERNANDEZ
    #Date: 14-12-2017
    #LastUpdate: 14-12-2017
    #Aim: Classify sequences
    #Use: mothur "#classify.seqs()"
    threads: 8
    log:
        'log/classify-seqs.log'
    params:
        outputdir=OUTPUTDIR, # output directory
        template=TEMPLATE,   # ref sequences
        taxonomy=TAXONOMY    # ref taxonomy
    message:
        'Classify sequences'
    input:
        SubsampleFasta='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fasta',
        SubsampleNames='out/mothur/{dataset}/{dataset}.trim.contigs.good.pick.subsample.names',
    output:
        WangTaxonomy='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.2017_taxonomy.wang.taxonomy',
        WangTaxSummary='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.2017_taxonomy.wang.tax.summary',
        WangFlipAccnos='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.2017_taxonomy.wang.flip.accnos'
    shell:
        'mothur \"# '
        'classify.seqs( '
        'fasta={input.SubsampleFasta}, '
        'name={input.SubsampleNames}, '
        'template={params.template}, '
        'taxonomy={params.taxonomy}, '
        'processors={threads}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'

############################################################################################

rule cluster:
    #Rule 13.0
    #Author: Nicolas FERNANDEZ
    #Date: 14-12-2017
    #LastUpdate: 14-12-2017
    #Aim: Sequences clusterization using distance matrix
    #Use: mothur "#cluster()"
    threads: 8
    log:
        'log/cluster.log'
    params:
        outputdir=OUTPUTDIR, # output directory
        method=METHOD,       # clustering start from furthest sequences ; can be change for 'nearest'
        showabund=SHOWABUND  # don't show each cluster in a file ; avoid computer memory overcharge
    message:
        'Sequences clusterization using distance matrix'
    input:
        DistanceMatrix='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.dist',
        SubsampleNames='out/mothur/{dataset}/{dataset}.trim.contigs.good.pick.subsample.names',
    output:
        FnSabund='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.sabund',
        FnRabund='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.rabund',
        FnList='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fn.list'
    shell:
        'mothur \"# '
        'cluster( '
        'column={input.DistanceMatrix}, '
        'name={input.SubsampleNames}, '
        'method={params.method}, '
        'showabund={params.showabund}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'

############################################################################################

rule pairwise_seqs:
    #Rule 12.0
    #Author: Nicolas FERNANDEZ
    #Date: 13-12-2017
    #LastUpdate: 14-12-2017
    #Aim: Compare sample vs sample to generate distance matrix
    #Use: mothur "#pairwise.seqs()"
    threads: 8
    log:
        'log/pairwise_seqs.log'
    params:
        outputdir=OUTPUTDIR, # output directory
        cutoff=CUTOFF,       # allow 10% error bases
        calc=CALC,           # missing bases are considered like one unique gap
        countends=COUNTENDS  # gaps in sequence end are ignored
    message:
        'Compare sample vs sample to generate distance matrix'
    input:
        SubsampleFasta='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fasta'
    output:
        DistanceMatrix='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.dist'
    shell:
        'mothur \"# '
        'pairwise.seqs( '
        'fasta={input.SubsampleFasta}, '
        'cutoff={params.cutoff}, '
        'calc={params.calc}, '
        'countends={params.countends}, '
        'processors={threads}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'

############################################################################################

rule subsample_count_groups:
    #Rule 11.0
    #Author: Nicolas FERNANDEZ
    #Date: 13-12-2017
    #LastUpdate: 13-12-2017
    #Aim: Count sequence number per samples
    #Use: mothur "#count.groups()"
    threads: 8
    log:
        'log/subsample-count-groups.log'
    params:
        outputdir=OUTPUTDIR # output directory
    message:
        'Count sequence number per samples'
    input:
        SubsampleGroups='out/mothur/{dataset}/{dataset}.contigs.good.pick.subsample.groups'
    output:
        SubsampleCountSummary='out/mothur/{dataset}/{dataset}.contigs.good.pick.subsample.count.summary'
    shell:
        'mothur \"# '
        'count.groups( '
        'group={input.SubsampleGroups}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'

############################################################################################

rule sub_sample:
    #Rule 10.0
    #Author: Nicolas FERNANDEZ
    #Date: 13-12-2017
    #LastUpdate: 13-12-2017
    #Aim: Sub-sampling each sample
    #Use: mothur "#sub.sample()"
    threads: 8
    log:
        'log/sub-sample.log'
    params:
        outputdir=OUTPUTDIR, # output directory
        persample=PERSAMPLE  # cf. variables
    message:
        'Sub-sampling each sample'
    input:
        PickFasta='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.fasta',
        PickGroup='out/mothur/{dataset}/{dataset}.contigs.good.pick.groups',
        PickNames='out/mothur/{dataset}/{dataset}.trim.contigs.good.pick.names'
    output:
        SubsampleNames='out/mothur/{dataset}/{dataset}.trim.contigs.good.pick.subsample.names',
        SubsampleFasta='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.subsample.fasta',
        SubsampleGroups='out/mothur/{dataset}/{dataset}.contigs.good.pick.subsample.groups'
    shell:
        'mothur \"# '
        'sub.sample( '
        'fasta={input.PickFasta}, '
        'group={input.PickGroup}, '
        'name={input.PickNames}, '
        'persample={params.persample}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'

############################################################################################

rule pick_groups_count:
    #Rule 9.0
    #Author: Nicolas FERNANDEZ
    #Date: 13-12-2017
    #LastUpdate: 13-12-2017
    #Aim: Count sequence number per samples
    #Use: mothur "#count.groups()"
    threads: 8
    log:
        'log/pick-groups-count.log'
    params:
        outputdir=OUTPUTDIR # output directory
    message:
        'Count sequence number per samples'
    input:
        PickGroup='out/mothur/{dataset}/{dataset}.contigs.good.pick.groups'
    output:
        PickCountSummary='out/mothur/{dataset}/{dataset}.contigs.good.pick.count.summary'
    shell:
        'mothur \"# '
        'count.groups( '
        'group={input.PickGroup}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'
    
############################################################################################
 
rule remove_seqs:
    #Rule 8.0
    #Author: Nicolas FERNANDEZ
    #Date: 13-12-2017
    #LastUpdate: 13-12-2017
    #Aim: Remove found chimeras sequences
    #Use: mothur "#remove.seqs()"
    threads: 8
    log:
        'log/remove-seqs.log'
    params:
        outputdir=OUTPUTDIR # output directory
    message:
        'Remove found chimeras sequences'
    input:
        UchimeAccnos='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.denovo.uchime.accnos',
        UniqueFasta='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.fasta',
        GoodName='out/mothur/{dataset}/{dataset}.trim.contigs.good.names',
        GoodGroup='out/mothur/{dataset}/{dataset}.contigs.good.groups'
    output:
        PickNames='out/mothur/{dataset}/{dataset}.trim.contigs.good.pick.names',
        PickFasta='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.pick.fasta',
        PickGroup='out/mothur/{dataset}/{dataset}.contigs.good.pick.groups'
    shell:
        'mothur \"# '
        'remove.seqs( '
        'accnos={input.UchimeAccnos}, '
        'fasta={input.UniqueFasta}, '
        'name={input.GoodName}, '
        'group={input.GoodGroup}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'

############################################################################################

rule chimera_uchime:
    #Rule 7.0
    #Author: Nicolas FERNANDEZ
    #Date: 13-12-2017
    #LastUpdate: 13-12-2017
    #Aim: Search for chimeras sequences
    #Use: mothur "#chimera.uchime()"
    threads: 8
    log:
        'log/chimera-uchime.log'
    params:
        outputdir=OUTPUTDIR, # output directory
        reference=REFERENCE  # cf. variables
    message:
        'Search for chimeras sequences'
    input:
        UniqueFasta='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.fasta',
        GoodName='out/mothur/{dataset}/{dataset}.trim.contigs.good.names'
    output:
        UchimeChimeras='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.denovo.uchime.chimeras',
        UchimeAccnos='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.denovo.uchime.accnos'
    shell:
        'mothur \"# '
        'chimera.uchime( '
        'fasta={input.UniqueFasta}, '
        'reference={params.reference}, '
        'name={input.GoodName}, '
        'processors={threads}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'

############################################################################################

rule unique_seqs:
    #Rule 6.0
    #Author: Nicolas FERNANDEZ
    #Date: 13-12-2017
    #LastUpdate: 13-12-2017
    #Aim: Group multiple similar sequences in one unique sequence
    #Use: mothur "#unique.seqs()"
    threads: 8
    log:
        'log/unique-seqs.log'
    params:
        outputdir=OUTPUTDIR # output directory
    message:
        'Group multiple similar sequences in one unique sequence'
    input:
        GoodFasta='out/mothur/{dataset}/{dataset}.trim.contigs.good.fasta'
    output:
        GoodName='out/mothur/{dataset}/{dataset}.trim.contigs.good.names',
        UniqueFasta='out/mothur/{dataset}/{dataset}.trim.contigs.good.unique.fasta'
    shell:
        'mothur \"# '
        'unique.seqs( '
        'fasta={input.GoodFasta}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'

############################################################################################

rule count_group:
    #Rule 5.0
    #Author: Nicolas FERNANDEZ
    #Date: 13-12-2017
    #LastUpdate: 13-12-2017
    #Aim: Count sequence number per samples
    #Use: mothur "#count.groups()"
    threads: 8
    log:
        'log/count-group.log'
    params:
        outputdir=OUTPUTDIR # output directory
    message:
        'Count sequence number per samples'
    input:
        GoodGroup='out/mothur/{dataset}/{dataset}.contigs.good.groups'
    output:
        CountSummary='out/mothur/{dataset}/{dataset}.contigs.good.count.summary'
    shell:
        'mothur \"# '
        'count.groups( '
        'group={input.GoodGroup}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'

############################################################################################

rule good_summary_seqs:
    #Rule 4.0
    #Author: Nicolas FERNANDEZ
    #Date: 13-12-2017
    #LastUpdate: 13-12-2017
    #Aim: Summarize the quality of sequences in a fasta-formatted sequence file
    #Use: mothur "#summary.seqs()"
    threads: 8
    log:
        'log/good-summary-seqs.log'
    params:
        outputdir=OUTPUTDIR # output directory
    message:
        'Summarize the quality of sequences from : {wildcards.dataset}'
    input:
        GoodFasta='out/mothur/{dataset}/{dataset}.trim.contigs.good.fasta'
    output:
        GoodSummary='out/mothur/{dataset}/{dataset}.trim.contigs.good.summary'
    shell:
        'mothur \"# '
        'summary.seqs( '
        'fasta={input.GoodFasta}, '
        'processors={threads}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'

############################################################################################

rule screen_seqs:
    #Rule 3.0
    #Author: Nicolas FERNANDEZ
    #Date: 13-12-2017
    #LastUpdate: 13-12-2017
    #Aim: Keep sequences that fulfill certain user defined criteria
    #Use: mothur "#screen.seqs()"
    threads: 8
    log:
        'log/screen-seqs.log'
    params:
        outputdir=OUTPUTDIR, # output directory
        maxambig=MAXAMBIG,   # max ambiguous bases allowed
        maxhomop=MAXHOMOP,   # max homopolymere allowed
        minlength=MINLENGTH  # min sequence size
    message:
        'Keep sequences from dataset {wildcards.dataset} that fulfill user defined criteria, '
        'Max ambigous bases allowed : {params.maxambig}, '
        'Max homopolymere allowed : {params.maxhomop}, '
        'Min length : {params.minlength}'
    input:
        ContigFasta='out/mothur/{dataset}/{dataset}.trim.contigs.fasta',
        ContigGroup='out/mothur/{dataset}/{dataset}.contigs.groups'
    output:
        GoodFasta='out/mothur/{dataset}/{dataset}.trim.contigs.good.fasta',
        BadAccnos='out/mothur/{dataset}/{dataset}.trim.contigs.bad.accnos',
        GoodGroup='out/mothur/{dataset}/{dataset}.contigs.good.groups'
    shell:
        'mothur \"# '
        'screen.seqs( '
        'fasta={input.ContigFasta}, '
        'group={input.ContigGroup}, '
        'maxambig={params.maxambig}, '
        'maxhomop={params.maxhomop}, '
        'minlength={params.minlength}, '
        'processors={threads}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'

############################################################################################

rule summary_seqs:
    #Rule 2.0
    #Author: Nicolas FERNANDEZ
    #Date: 13-12-2017
    #LastUpdate: 13-12-2017
    #Aim: Summarize the quality of sequences in a fasta-formatted sequence file
    #Use: mothur "#summary.seqs()"
    threads: 8
    log:
       'log/summary-seqs.log'
    params:
        outputdir=OUTPUTDIR # output directory
    message:
        'Summarize the quality of sequences from : {wildcards.dataset}'
    input:
        ContigFasta='out/mothur/{dataset}/{dataset}.trim.contigs.fasta'
    output:
        ContigSummary='out/mothur/{dataset}/{dataset}.trim.contigs.summary'
    shell:
        'mothur \"# '
        'summary.seqs( '
        'fasta={input.ContigFasta}, '
        'processors={threads}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'

############################################################################################

rule make_contigs:
    #Rule 1.0
    #Author: Nicolas FERNANDEZ
    #Date: 13-12-2017
    #LastUpdate: 13-12-2017
    #Aim: Read a forward fastq file and a reverse fastq file and outputs new fasta and report files
    #Use: mothur "#make.contigs()"
    threads: 8
    log:
       'log/make-contigs.log'
    params:
        outputdir=OUTPUTDIR # output directory
    message:
        'Read forward fastq file and a reverse fastq file for dataset : {wildcards.dataset}'
    input:
        ImportFile='inp/mothur/{dataset}.file'
    output:
        ContigFasta='out/mothur/{dataset}/{dataset}.trim.contigs.fasta',
        ContigQual='out/mothur/{dataset}/{dataset}.contigs.qual',
        ContigReport='out/mothur/{dataset}/{dataset}.contigs.report',
        ScrapContigFasta='out/mothur/{dataset}/{dataset}.scrap.contigs.fasta',
        ScrapContigQual='out/mothur/{dataset}/{dataset}.scrap.contigs.qual',
        ContigGroup='out/mothur/{dataset}/{dataset}.contigs.groups'
    shell:
        'mothur \"# '
        'make.contigs( '
        'file={input.ImportFile}, '
        'processors={threads}, '
        'outputdir=./out/mothur/{wildcards.dataset}) '
        '\"'
        
############################################################################################
########################################## MOTHUR #########################################
############################################################################################
